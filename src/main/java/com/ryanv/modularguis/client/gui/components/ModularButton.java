package com.ryanv.modularguis.client.gui.components;

import net.minecraft.client.gui.GuiButton;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModularButton extends GuiButton {

    public ModularButton(int id, int x, int y, String buttonText) {
        super(id, x, y, buttonText);
    }

    public ModularButton(int id, int x, int y, int width, int height, String buttonText) {
        super(id, x, y, width, height, buttonText);
    }

}
