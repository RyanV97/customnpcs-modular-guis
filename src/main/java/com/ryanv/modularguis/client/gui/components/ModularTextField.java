package com.ryanv.modularguis.client.gui.components;

import com.ryanv.modularguis.api.client.gui.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.nbt.NBTTagCompound;

public class ModularTextField extends GuiTextField implements IDataHolder, IRenderable, IClickListener, IKeyListener {

    public ModularTextField(int componentId, int x, int y, int width, int height) {
        super(componentId, Minecraft.getMinecraft().fontRenderer, x, y, width, height);
    }

    @Override
    public void onRender(IGUI gui, Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        this.drawTextBox();
    }

    @Override
    public boolean mouseClicked(int mouseX, int mouseY, int mouseButton) {
        return super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void keyTyped(char typedChar, int keyCode) {
        super.textboxKeyTyped(typedChar, keyCode);
    }

    @Override
    public NBTTagCompound toNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("type", 3);
        tag.setInteger("id", this.getId());
        tag.setString("text", this.getText());
        return tag;
    }
}
