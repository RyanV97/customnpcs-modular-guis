package com.ryanv.modularguis.client.gui.components;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.client.gui.IRenderable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.ResourceLocation;

public class ModularTexturedRect extends Gui implements IRenderable {

    ResourceLocation texture;
    int x,y,width,height,textureX,textureY;

    public ModularTexturedRect(String texture, int x, int y, int width, int height, int textureX, int textureY) {
        this.texture = new ResourceLocation(texture);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.textureX = textureX;
        this.textureY = textureY;
    }

    @Override
    public void onRender(IGUI gui, Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        mc.getTextureManager().bindTexture(texture);
        drawTexturedModalRect(x,y, textureX,textureY, width,height);
    }
}
