package com.ryanv.modularguis.client.gui;

import com.ryanv.modularguis.ModularGuis;
import com.ryanv.modularguis.api.client.gui.*;
import com.ryanv.modularguis.api.scripting.gui.IModularComponent;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import com.ryanv.modularguis.network.ModularButtonClickPacket;
import com.ryanv.modularguis.network.ModularGUIClosePacket;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@SideOnly(Side.CLIENT)
public class ModularScreen extends GuiScreen implements IGUI {

    IModularGUI gui;
    int xSize,ySize;
    ResourceLocation background;

    List<IRenderable> renderList = new ArrayList<>();
    List<IClickListener> clickListeners = new ArrayList<>();
    List<IKeyListener> keyListeners = new ArrayList<>();
    List<IDataHolder> dataHolders = new ArrayList<>();

    public ModularScreen(IModularGUI gui) {
        this.gui = gui;
        xSize = gui.getWidth();
        ySize = gui.getHeight();
        if(!gui.getBackgroundTexture().isEmpty())
            background = new ResourceLocation(gui.getBackgroundTexture());
    }

    @Override
    public void initGui() {
        super.initGui();
        renderList.clear();
        for(IModularComponent component : gui.getComponents()) {
            component.addToGUI(this);
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
        for(IDataHolder component : dataHolders) {
            if(component instanceof GuiTextField)
                ((GuiTextField) component).updateCursorCounter();
        }
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        drawDefaultBackground();
        if(background!=null)
            drawBackgroundTexture();
        for(IRenderable component : renderList) {
            component.onRender(this, mc, mouseX, mouseY, partialTicks);
        }
        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    void drawBackgroundTexture() {
        mc.getTextureManager().bindTexture(background);
        drawTexturedModalRect(getLeft(), getTop(), 0,0, xSize, ySize);
    }

    @Override
    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        NBTTagList list = new NBTTagList();
        for(IDataHolder component : dataHolders)
            list.appendTag(component.toNBT());
        NBTTagCompound data = new NBTTagCompound();
        data.setTag("data", list);
        ModularGuis.CHANNEL.sendToServer(new ModularButtonClickPacket(gui.getID(), button.id, data));
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        super.keyTyped(typedChar, keyCode);
        for(IKeyListener listener : this.keyListeners)
            listener.keyTyped(typedChar, keyCode);
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        for(IClickListener listener : clickListeners)
            listener.mouseClicked(mouseX, mouseY, mouseButton);
    }

    @Override
    public void onGuiClosed() {
        super.onGuiClosed();
        NBTTagList list = new NBTTagList();
        for(IDataHolder component : dataHolders)
            list.appendTag(component.toNBT());
        NBTTagCompound data = new NBTTagCompound();
        data.setTag("data", list);
        ModularGuis.CHANNEL.sendToServer(new ModularGUIClosePacket(gui.getID(), data));
    }

    @Override
    public boolean doesGuiPauseGame() {
        return gui.getDoesPauseGame();
    }

    @Override
    public Gui getGUI() {
        return this;
    }

    @Override
    public int getLeft() {
        return (this.width - this.xSize) / 2;
    }

    @Override
    public int getTop() {
        return (this.height - this.ySize) / 2;
    }

    @Override
    public void addBtn(GuiButton button) {
        this.addButton(button);
    }

    @Override
    public void addRender(IRenderable component) {
        this.renderList.add(component);
    }

    @Override
    public void addDataHolder(IDataHolder component) {
        this.dataHolders.add(component);
    }

    @Override
    public void addKeyListener(IKeyListener component) {
        this.keyListeners.add(component);
    }

    @Override
    public void addClickListener(IClickListener component) {
        this.clickListeners.add(component);
    }


}