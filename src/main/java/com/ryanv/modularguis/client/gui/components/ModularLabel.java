package com.ryanv.modularguis.client.gui.components;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.client.gui.IRenderable;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiLabel;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModularLabel extends GuiLabel implements IRenderable {

    public ModularLabel(int id, String label, int x, int y, int width, int height, int colour) {
        super(Minecraft.getMinecraft().fontRenderer, id, x, y, width, height, colour);
        addLine(label);
    }

    @Override
    public void onRender(IGUI gui, Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        this.drawLabel(mc, mouseX, mouseY);
    }

}
