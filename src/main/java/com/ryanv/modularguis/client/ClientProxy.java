package com.ryanv.modularguis.client;

import com.ryanv.modularguis.CommonProxy;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import com.ryanv.modularguis.client.gui.ModularScreen;
import net.minecraft.client.Minecraft;
import noppes.npcs.client.NoppesUtil;

public class ClientProxy extends CommonProxy {

    public void openGui(IModularGUI gui) {
        NoppesUtil.openGUI(Minecraft.getMinecraft().player, new ModularScreen(gui));
    };

}
