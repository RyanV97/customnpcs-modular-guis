package com.ryanv.modularguis;

import com.ryanv.modularguis.api.events.GuiEvent;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.MinecraftForge;
import noppes.npcs.api.wrapper.NBTWrapper;
import noppes.npcs.constants.EnumScriptType;
import noppes.npcs.controllers.IScriptHandler;
import noppes.npcs.controllers.data.ForgeScriptData;
import org.apache.commons.lang3.StringUtils;

public class EventHooks {

    /** Add more specific EnumScriptType for each GUI event */

    public static void onGuiClosed(final IScriptHandler handler, IModularGUI gui, NBTWrapper[] data, EntityPlayerMP player) {
        final GuiEvent.CloseEvent event = new GuiEvent.CloseEvent(gui.getID(), player, data);
        handler.runScript(EnumScriptType.CONTAINER_CLOSED, event);
        MinecraftForge.EVENT_BUS.post(event);
    }

    public static void onGuiButton(final IScriptHandler handler, IModularGUI gui, int buttonId, NBTWrapper[] data, EntityPlayerMP player) {
        final GuiEvent.ButtonEvent event = new GuiEvent.ButtonEvent(gui.getID(), buttonId, player, data);
        handler.runScript(EnumScriptType.CLICKED, event);
        MinecraftForge.EVENT_BUS.post(event);
    }

}
