package com.ryanv.modularguis;

import com.ryanv.modularguis.network.ModularButtonClickPacket;
import com.ryanv.modularguis.network.ModularGUIClosePacket;
import com.ryanv.modularguis.network.OpenModularPacket;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = ModularGuis.MOD_ID, name = ModularGuis.MOD_NAME, version = ModularGuis.VERSION, dependencies = "required-after:customnpcs")
public class ModularGuis {

    public static final String MOD_ID = "modular-guis";
    public static final String MOD_NAME = "Modular Guis";
    public static final String VERSION = "2019.3-1.3.2";

    public static final SimpleNetworkWrapper CHANNEL = NetworkRegistry.INSTANCE.newSimpleChannel(MOD_ID);

    @Mod.Instance(MOD_ID)
    public static ModularGuis INSTANCE;

    @SidedProxy(modId = MOD_ID, serverSide = "com.ryanv.modularguis.CommonProxy", clientSide = "com.ryanv.modularguis.client.ClientProxy")
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        CHANNEL.registerMessage(OpenModularPacket.Handler.class, OpenModularPacket.class, 0, Side.CLIENT);
        CHANNEL.registerMessage(ModularButtonClickPacket.Handler.class, ModularButtonClickPacket.class, 1, Side.SERVER);
        CHANNEL.registerMessage(ModularGUIClosePacket.Handler.class, ModularGUIClosePacket.class, 2, Side.SERVER);
    }

}
