package com.ryanv.modularguis.network;

import com.ryanv.modularguis.ModularGuis;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import com.ryanv.modularguis.api.scripting.wrapper.GUIWrapper;
import com.ryanv.modularguis.client.gui.ModularScreen;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import noppes.npcs.client.NoppesUtil;

public class OpenModularPacket implements IMessage {

    /** Packet sent from Server -> Client, with the GUI Information for opening a new GUI. */

    IModularGUI gui;

    public OpenModularPacket(){}
    public OpenModularPacket(IModularGUI gui) {
        this.gui = gui;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        gui.toBytes(buf);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        gui = new GUIWrapper(ByteBufUtils.readTag(buf));
    }

    public static class Handler implements IMessageHandler<OpenModularPacket, IMessage> {

        /** I use Proxy here just to run the openGui method in ClientProxy when on the Client.
         *  For some reason I was having issues if that code was anywhere else despite this only ever running on the Client.
         **/

        @Override
        public IMessage onMessage(OpenModularPacket message, MessageContext ctx) {
            Minecraft.getMinecraft().addScheduledTask(() -> ModularGuis.proxy.openGui(message.gui));
            return null;
        }

    }

}
