package com.ryanv.modularguis.network;

import com.ryanv.modularguis.ModularGuis;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import noppes.npcs.api.wrapper.NBTWrapper;

public class ModularButtonClickPacket implements IMessage {

    /** Sent from Client -> Server when a ModularGUI Button is clicked, with the guiID, buttonId, and data from all DataHolder components, to create a new ButtonClicked event. */

    int guiId;
    int buttonId;
    NBTTagCompound data;

    public ModularButtonClickPacket() {}
    public ModularButtonClickPacket(int guiId, int buttonId, NBTTagCompound data) {
        this.guiId = guiId;
        this.buttonId = buttonId;
        this.data = data;
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(guiId);
        buf.writeInt(buttonId);
        ByteBufUtils.writeTag(buf, data);
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        guiId = buf.readInt();
        buttonId = buf.readInt();
        data = ByteBufUtils.readTag(buf);
    }

    public static class Handler implements IMessageHandler<ModularButtonClickPacket, IMessage> {

        @Override
        public IMessage onMessage(ModularButtonClickPacket message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServerWorld().addScheduledTask(() -> {
                NBTTagList list = message.data.getTagList("data", Constants.NBT.TAG_COMPOUND);
                NBTWrapper[] data = new NBTWrapper[list.tagCount()];
                for(int i = 0; i < list.tagCount(); i++) {
                    data[i] = new NBTWrapper((NBTTagCompound) list.get(i));
                }
                ModularGuis.proxy.onModularButtonClick(message.guiId, message.buttonId, data, player);
            });
            return null;
        }

    }

}
