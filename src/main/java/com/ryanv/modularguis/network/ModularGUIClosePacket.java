package com.ryanv.modularguis.network;

import com.ryanv.modularguis.ModularGuis;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import noppes.npcs.api.wrapper.NBTWrapper;

public class ModularGUIClosePacket implements IMessage {

    /** Sent from Client -> Server when a Modular GUI closes, with the guiID, and data from all DataHolder components, to create a new ButtonClosed event. */

    int guiID;
    NBTTagCompound data;

    public ModularGUIClosePacket(){}
    public ModularGUIClosePacket(int guiID, NBTTagCompound data) {
        this.guiID = guiID;
        this.data = data;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.guiID = buf.readInt();
        this.data = ByteBufUtils.readTag(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(guiID);
        ByteBufUtils.writeTag(buf, data);
    }

    public static class Handler implements IMessageHandler<ModularGUIClosePacket, IMessage> {

        @Override
        public IMessage onMessage(ModularGUIClosePacket message, MessageContext ctx) {
            EntityPlayerMP player = ctx.getServerHandler().player;
            player.getServerWorld().addScheduledTask(() -> {
                NBTTagList list = message.data.getTagList("data", Constants.NBT.TAG_COMPOUND);
                NBTWrapper[] data = new NBTWrapper[list.tagCount()];
                for (int i = 0; i < list.tagCount(); i++) {
                    data[i] = new NBTWrapper((NBTTagCompound) list.get(i));
                }
                ModularGuis.proxy.onModularGuiClose(message.guiID, data, player);
            });
            return null;
        }

    }

}
