package com.ryanv.modularguis.api.events;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.Cancelable;
import noppes.npcs.api.NpcAPI;
import noppes.npcs.api.event.CustomNPCsEvent;
import noppes.npcs.api.wrapper.NBTWrapper;

import java.lang.reflect.Field;

@Cancelable
public class GuiEvent extends CustomNPCsEvent {

    EntityPlayerMP player;
    int guiID;
    NBTWrapper[] data;

    public GuiEvent() {}

    public GuiEvent(int guiID, EntityPlayerMP player, NBTWrapper[] data) {
        this.guiID = guiID;
        this.player = player;
        this.data = data;
    }

    public static class CloseEvent extends GuiEvent {
        public CloseEvent(int guiID, EntityPlayerMP player, NBTWrapper[] data) {
            super(guiID, player, data);
        }
    }

    public static class ButtonEvent extends GuiEvent {

        int buttonId;

        public ButtonEvent(int guiID, int buttonId, EntityPlayerMP player, NBTWrapper[] data) {
            super(guiID, player, data);
            this.buttonId = buttonId;
        }

    }

}
