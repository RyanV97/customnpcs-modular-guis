package com.ryanv.modularguis.api.scripting.wrapper;

import com.ryanv.modularguis.api.scripting.gui.IModularComponent;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUIButtonWrapper;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUILabelWrapper;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUITextFieldWrapper;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUITexturedRectWrapper;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import noppes.npcs.controllers.IScriptHandler;

import java.util.ArrayList;
import java.util.List;

public class GUIWrapper implements IModularGUI {

    IScriptHandler parent;

    int id;
    List<IModularComponent> components = new ArrayList<>();
    int width,height;
    boolean pauseGame;
    String backgroundTexture;

    public GUIWrapper(int id, int width, int height) {
        this(id, width, height, true);
    }

    public GUIWrapper(int id, int width, int height, boolean pauseGame) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.pauseGame = pauseGame;
    }

    public GUIWrapper(NBTTagCompound tag) {
        this.width = tag.getIntArray("size")[0];
        this.height = tag.getIntArray("size")[1];
        this.pauseGame = tag.getBoolean("pauseGame");
        this.backgroundTexture = tag.getString("bgTexture");
        for(NBTBase c : tag.getTagList("components", Constants.NBT.TAG_COMPOUND)) {
            this.components.add(IModularComponent.fromNBT((NBTTagCompound)c));
        }
    }

    @Override
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public void setDoesPauseGame(boolean pauseGame) {
        this.pauseGame = pauseGame;
    }

    @Override
    public void setBackgroundTexture(String resourceLocation) {
        this.backgroundTexture = resourceLocation;
    }

    @Override
    public String getBackgroundTexture() {
        return this.backgroundTexture;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public IScriptHandler getParent() {
        return this.parent;
    }

    @Override
    public void setParent(IScriptHandler handler) {
        this.parent = handler;
    }

    @Override
    public void addButton(int id, String label, int x, int y) {
        this.components.add(new GUIButtonWrapper(id, label, x, y));
    }

    @Override
    public void addButton(int id, String label, int x, int y, int width, int height) {
        this.components.add(new GUIButtonWrapper(id, label, x, y, width, height));
    }

    @Override
    public List<IModularComponent> getComponents() {
        return this.components;
    }

    @Override
    public void addLabel(int id, String label, int x, int y, int width, int height) {
        this.components.add(new GUILabelWrapper(id, label, x, y, width, height));
    }

    @Override
    public void addLabel(int id, String label, int x, int y, int width, int height, int color) {
        this.components.add(new GUILabelWrapper(id, label, x, y, width, height, color));
    }

    @Override
    public void addTextField(int id, int x, int y, int width, int height) {
        this.components.add(new GUITextFieldWrapper(id,x,y,width,height));
    }

    @Override
    public void addTexturedRect(String texture, int x, int y, int width, int height) {
        this.components.add(new GUITexturedRectWrapper(texture, x, y, width, height));
    }

    @Override
    public void addTexturedRect(String texture, int x, int y, int width, int height, int textureX, int textureY) {
        this.components.add(new GUITexturedRectWrapper(texture, x, y, width, height, textureX, textureY));
    }

    @Override
    public void toBytes(ByteBuf buf) {
        NBTTagCompound tag = new NBTTagCompound();

        tag.setIntArray("size", new int[]{this.width, this.height});
        tag.setBoolean("pauseGame", pauseGame);
        tag.setString("bgTexture", backgroundTexture);

        NBTTagList compList = new NBTTagList();
        for(IModularComponent c : this.components) {
            compList.appendTag(c.toNBT());
        }
        tag.setTag("components", compList);

        ByteBufUtils.writeTag(buf, tag);
    }

    @Override
    public boolean getDoesPauseGame() {
        return pauseGame;
    }

}
