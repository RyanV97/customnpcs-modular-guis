package com.ryanv.modularguis.api.scripting.wrapper.components;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.scripting.gui.IModularComponent;
import com.ryanv.modularguis.client.gui.components.ModularLabel;
import net.minecraft.nbt.NBTTagCompound;

public class GUILabelWrapper implements IModularComponent {

    int id;
    String label;
    int x,y,width,height;
    int color;

    public GUILabelWrapper(int id, String label, int x, int y, int width, int height) {
        this(id, label, x, y, width, height, 0xffffff);
    }

    public GUILabelWrapper(int id, String label, int x, int y, int width, int height, int color) {
        this.id = id;
        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
    }

    public GUILabelWrapper(NBTTagCompound tag) {
        this.id = tag.getInteger("id");
        this.label = tag.getString("label");
        this.x = tag.getIntArray("pos")[0];
        this.y = tag.getIntArray("pos")[1];
        this.width = tag.getIntArray("size")[0];
        this.height = tag.getIntArray("size")[1];
        this.color = tag.getInteger("color");
    }

    public NBTTagCompound toNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("type", 1);
        tag.setInteger("id", id);
        tag.setString("label", label);
        tag.setIntArray("pos", new int[]{x,y});
        tag.setIntArray("size", new int[]{width,height});
        tag.setInteger("color", color);
        return tag;
    }

    @Override
    public void addToGUI(IGUI gui) {
        gui.addRender(new ModularLabel(id, label, gui.getLeft() + x, gui.getTop() + y, width, height, color));
    }

}
