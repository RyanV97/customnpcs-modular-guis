package com.ryanv.modularguis.api.scripting.wrapper.components;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.scripting.gui.IModularComponent;
import com.ryanv.modularguis.client.gui.components.ModularTexturedRect;
import net.minecraft.nbt.NBTTagCompound;

public class GUITexturedRectWrapper implements IModularComponent {

    String texture;
    int x,y,width,height,textureX,textureY;

    public GUITexturedRectWrapper(String texture, int x, int y, int width, int height) {
        this(texture,x,y,width,height,0,0);
    }

    public GUITexturedRectWrapper(String texture, int x, int y, int width, int height, int textureX, int textureY) {
        this.texture = texture;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.textureX = textureX;
        this.textureY = textureY;
    }

    public GUITexturedRectWrapper(NBTTagCompound tag) {
        this.texture = tag.getString("texture");
        this.x = tag.getIntArray("pos")[0];
        this.y = tag.getIntArray("pos")[1];
        this.width = tag.getIntArray("size")[0];
        this.height = tag.getIntArray("size")[1];
        this.textureX = tag.getIntArray("texPos")[0];
        this.textureY = tag.getIntArray("texPos")[1];
    }

    @Override
    public NBTTagCompound toNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("type",2);
        tag.setString("texture", texture);
        tag.setIntArray("pos", new int[]{x,y});
        tag.setIntArray("size", new int[]{width,height});
        tag.setIntArray("texPos", new int[]{textureX,textureY});
        return tag;
    }

    @Override
    public void addToGUI(IGUI gui) {
        gui.addRender(new ModularTexturedRect(texture, gui.getLeft() + x, gui.getTop() + y, width,height,textureX,textureY));
    }

}
