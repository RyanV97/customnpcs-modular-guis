package com.ryanv.modularguis.api.scripting.wrapper.components;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.scripting.gui.IModularComponent;
import com.ryanv.modularguis.client.gui.components.ModularButton;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class GUIButtonWrapper implements IModularComponent {

    int id;
    String label;
    int x,y,width,height;

    public GUIButtonWrapper(int id, String label, int x, int y) {
        this(id, label, x, y, 200, 20);
    }

    public GUIButtonWrapper(int id, String label, int x, int y, int width, int height) {
        this.id = id;
        this.label = label;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public GUIButtonWrapper(NBTTagCompound tag) {
        this.id = tag.getInteger("id");
        this.label = tag.getString("label");
        this.x = tag.getIntArray("pos")[0];
        this.y = tag.getIntArray("pos")[1];
        this.width = tag.getIntArray("size")[0];
        this.height = tag.getIntArray("size")[1];
    }

    public NBTTagCompound toNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("type", 0);
        tag.setInteger("id", id);
        tag.setString("label", label);
        tag.setIntArray("pos", new int[]{x,y});
        tag.setIntArray("size", new int[]{width,height});
        return tag;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addToGUI(IGUI gui) {
        gui.addBtn(new ModularButton(id, gui.getLeft() + x, gui.getTop() + y, width, height, label));
    }


}
