package com.ryanv.modularguis.api.scripting.wrapper.components;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.scripting.gui.IModularComponent;
import com.ryanv.modularguis.client.gui.components.ModularTextField;
import net.minecraft.nbt.NBTTagCompound;

public class GUITextFieldWrapper implements IModularComponent {

    int id;
    int x,y,width,height;

    public GUITextFieldWrapper(int id, int x, int y, int width, int height) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public GUITextFieldWrapper(NBTTagCompound tag) {
        this.id = tag.getInteger("id");
        this.x = tag.getIntArray("pos")[0];
        this.y = tag.getIntArray("pos")[1];
        this.width = tag.getIntArray("size")[0];
        this.height = tag.getIntArray("size")[1];
    }

    public NBTTagCompound toNBT() {
        NBTTagCompound tag = new NBTTagCompound();
        tag.setInteger("type", 3);
        tag.setInteger("id", id);
        tag.setIntArray("pos", new int[]{x,y});
        tag.setIntArray("size", new int[]{width,height});
        return tag;
    }

    @Override
    public void addToGUI(IGUI gui) {
        ModularTextField component = new ModularTextField(id, gui.getLeft() + x, gui.getTop() + y, width,height);
        gui.addDataHolder(component);
        gui.addRender(component);
        gui.addKeyListener(component);
        gui.addClickListener(component);
    }
}
