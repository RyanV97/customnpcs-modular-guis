package com.ryanv.modularguis.api.scripting.gui;

import com.ryanv.modularguis.api.client.gui.IGUI;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUIButtonWrapper;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUILabelWrapper;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUITextFieldWrapper;
import com.ryanv.modularguis.api.scripting.wrapper.components.GUITexturedRectWrapper;
import net.minecraft.nbt.NBTTagCompound;

public interface IModularComponent {

    NBTTagCompound toNBT();
    void addToGUI(IGUI gui);

    static IModularComponent fromNBT(NBTTagCompound tag) {
        switch (tag.getInteger("type")) {
            case 0:
                return new GUIButtonWrapper(tag);
            case 1:
                return new GUILabelWrapper(tag);
            case 2:
                return new GUITexturedRectWrapper(tag);
            case 3:
                return new GUITextFieldWrapper(tag);
        }
        return null;
    }

}
