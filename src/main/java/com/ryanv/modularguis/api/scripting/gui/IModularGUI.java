package com.ryanv.modularguis.api.scripting.gui;

import io.netty.buffer.ByteBuf;
import noppes.npcs.controllers.IScriptHandler;

import java.util.List;

public interface IModularGUI {

    int getID();
    int getWidth();
    int getHeight();
    List<IModularComponent> getComponents();

    IScriptHandler getParent();
    void setParent(IScriptHandler handler);

    void setSize(int width, int height);

    void setDoesPauseGame(boolean pauseGame);
    boolean getDoesPauseGame();

    void setBackgroundTexture(String resourceLocation);
    String getBackgroundTexture();

    void addButton(int id, String label, int x, int y);
    void addButton(int id, String label, int x, int y, int width, int height);

    void addLabel(int id, String label, int x, int y, int width, int height);
    void addLabel(int id, String label, int x, int y, int width, int height, int color);

    void addTextField(int id, int x, int y, int width, int height);

    void addTexturedRect(String texture, int x, int y, int width, int height);
    void addTexturedRect(String texture, int x, int y, int width, int height, int textureX, int textureY);


    void toBytes(ByteBuf buf);

}
