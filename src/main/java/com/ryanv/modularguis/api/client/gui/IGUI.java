package com.ryanv.modularguis.api.client.gui;

import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiButton;

public interface IGUI {

    Gui getGUI();

    int getLeft();
    int getTop();

    void addBtn(GuiButton button);
    void addRender(IRenderable component);
    void addDataHolder(IDataHolder component);
    void addKeyListener(IKeyListener component);
    void addClickListener(IClickListener component);

}
