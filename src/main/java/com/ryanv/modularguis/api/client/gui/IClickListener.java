package com.ryanv.modularguis.api.client.gui;

public interface IClickListener {

    boolean mouseClicked(int mouseX, int mouseY, int mouseButton);

}
