package com.ryanv.modularguis.api.client.gui;

public interface IKeyListener {

    void keyTyped(char typedChar, int keyCode);

}
