package com.ryanv.modularguis.api.client.gui;

import net.minecraft.client.Minecraft;

public interface IRenderable {

    void onRender(IGUI gui, Minecraft mc, int mouseX, int mouseY, float partialTicks);

}
