package com.ryanv.modularguis.api.client.gui;

import net.minecraft.nbt.NBTTagCompound;

public interface IDataHolder {

    NBTTagCompound toNBT();

}
