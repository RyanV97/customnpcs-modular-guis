package com.ryanv.modularguis.api;

import com.ryanv.modularguis.ModularGuis;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import com.ryanv.modularguis.api.scripting.wrapper.GUIWrapper;
import com.ryanv.modularguis.network.OpenModularPacket;
import net.minecraft.entity.player.EntityPlayerMP;
import noppes.npcs.api.wrapper.BlockScriptedWrapper;
import noppes.npcs.api.wrapper.ItemScriptedWrapper;
import noppes.npcs.api.wrapper.NPCWrapper;
import noppes.npcs.api.wrapper.PlayerWrapper;
import noppes.npcs.blocks.tiles.TileScripted;
import noppes.npcs.controllers.IScriptHandler;
import noppes.npcs.controllers.ScriptController;
import noppes.npcs.entity.EntityNPCInterface;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class GuiAPI {

    public static Map<EntityPlayerMP, IModularGUI> openGUIS = new HashMap<>();

    public static IModularGUI createGui(int id, int width, int height) {
        return new GUIWrapper(id, width, height);
    }

    /** Open a given GUI for the given player.
     *  Arguments:
     *  parent - Scripted object that should have the GUI Events sent to it's script. (ScriptedBlocks, NPCs, ScriptedItems, etc.)
     *  player - The player to show this gui to.
     *  gui - The GUI to display.
     **/

    public static void openGui(Object parent, PlayerWrapper player, IModularGUI gui) {
        IScriptHandler handler = ScriptController.Instance.forgeScripts;
        if (parent instanceof BlockScriptedWrapper) {
            try {
                Field f = BlockScriptedWrapper.class.getDeclaredField("tile");
                f.setAccessible(true);
                handler = (TileScripted) f.get(parent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (parent instanceof ItemScriptedWrapper) {
            handler = (ItemScriptedWrapper) parent;
        } else if (parent instanceof PlayerWrapper) {
            handler = ScriptController.Instance.playerScripts;
        } else if (parent instanceof NPCWrapper) {
            handler = ((EntityNPCInterface) ((NPCWrapper) parent).getMCEntity()).script;
        }
        gui.setParent(handler);
        openGUIS.put((EntityPlayerMP) player.getMCEntity(), gui);
        ModularGuis.CHANNEL.sendTo(new OpenModularPacket(gui), (EntityPlayerMP) player.getMCEntity());
    }

}
