package com.ryanv.modularguis;

import com.ryanv.modularguis.api.GuiAPI;
import com.ryanv.modularguis.api.scripting.gui.IModularGUI;
import com.ryanv.modularguis.api.scripting.wrapper.GUIWrapper;
import com.ryanv.modularguis.network.OpenModularPacket;
import net.minecraft.entity.player.EntityPlayerMP;
import noppes.npcs.api.wrapper.*;
import noppes.npcs.blocks.tiles.TileScripted;
import noppes.npcs.controllers.IScriptHandler;
import noppes.npcs.controllers.ScriptController;
import noppes.npcs.entity.EntityNPCInterface;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class CommonProxy {

    public void openGui(IModularGUI gui){};

    public void onModularGuiClose(int guiID, NBTWrapper[] data, EntityPlayerMP player) {
        /** Check Player's Open GUI ID matches Packet's ID. For Security. */
        if(GuiAPI.openGUIS.containsKey(player)) {
            IModularGUI gui = GuiAPI.openGUIS.get(player);
            if (gui.getID() == guiID) {
                EventHooks.onGuiClosed(gui.getParent(), gui, data, player);
            }
        }
    }
    public void onModularButtonClick(int guiID, int buttonId, NBTWrapper[] data, EntityPlayerMP player) {
        /** Check Player's Open GUI ID matches Packet's ID. For Security. */
        if(GuiAPI.openGUIS.containsKey(player)) {
            IModularGUI gui = GuiAPI.openGUIS.get(player);
            if (gui.getID() == guiID) {
                EventHooks.onGuiButton(gui.getParent(), gui, buttonId, data, player);
            }
        }
    }

}
